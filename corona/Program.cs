﻿using System;
using System.IO;
using System.Linq;

//database file zal komen in (ongeveer): C:\Users\arnoa\source\repos\corona\corona\bin\Debug\database.txt

namespace corona
{
    class Program
    {


        private static bool IsDitEenDatum(string mijnString)
        {
            try
            {
                Convert.ToDateTime(mijnString);
                return true;
            }
            catch
            {
                return false;
            }

        }
        
        private static bool IsDitEenRijksnummer(string mijnString)
        {
            //format: 99090945667
            //mss 2 + geb datum(6 cijfers) + - + 3 cijfers + 2 cijfers

            try
            {
                int eersteCijfers = 0;
                int laatsteTwee = 0;
                if (mijnString[0] == '2' && "299090945667".Length == mijnString.Length)
                {
                    eersteCijfers = Convert.ToInt32(mijnString.Substring(0, 10));
                    laatsteTwee = Convert.ToInt32(mijnString.Substring(10, 2));
                }
                else
                {
                    eersteCijfers = Convert.ToInt32(mijnString.Substring(0, 9));
                    laatsteTwee = Convert.ToInt32(mijnString.Substring(9, 2));
                }
                

                if (97 - (eersteCijfers % 97)  == laatsteTwee)
                {
                    return true;
                }
                else
                {
                    return false;
                }

                
            }
            catch
            {
                return false;
            }

        }
        

        private static bool ArrayBevat(string element, string[] array)
        {
            bool gevonden = false;
            for (int i = 0; i < array.Length; i++)
            {
                if (element == array[i])
                {
                    gevonden = true;
                }
            }
            return gevonden;
        }

        private static double Gemiddelde(double[] array)
        {
            double totaal = 0;
            for (int i = 0; i < array.Length; i++)
            {
                totaal += array[i];
            }
            return Math.Round(totaal / array.Length,1);
        }

        private static void RegelMooiWeergeven(string regel)
        {
            //vb regel ="sqdf sdqf|01/01/1999|01/01/1999|75041502165|sqfgv|ziek|gebroken been, misselijk, allergisch aan vanalles"
            string[] eigenschappen = { "naam", "geboortedatum", "datum van opname", "rijksregisternummer", "naam van het ziekenhuis", "status", "opmerkingen" };

            char[] delimiterChars = { '|' };
            string[] regelArray = regel.Split(delimiterChars);

            Console.WriteLine();
            for (int i = 0; i < eigenschappen.Length; i++)
            {
                Console.WriteLine(eigenschappen[i]+": " + regelArray[i]);
            }
            Console.WriteLine();
        }


        private static void PatientToevoegen()
        {
            

            Console.WriteLine("\nWat is de naam van de patient?");
            bool doorvragen = true;
            string naam = "";
            while (doorvragen)
            {
                naam = Console.ReadLine();
                if (naam.Length>3 && naam.Contains(" "))
                {
                    doorvragen = false;
                }
                else
                {
                    Console.WriteLine("Dit is geen geldige naam. Probeer  opnieuw.");
                }
            }

            Console.WriteLine("\nWat is de geboortedatum?");
            doorvragen = true;
            string geboortedatum = "";
            while (doorvragen)
            {
                geboortedatum = Console.ReadLine();
                if (IsDitEenDatum(geboortedatum))
                {
                    double verschil = (DateTime.Now - Convert.ToDateTime(geboortedatum)).TotalDays / 365.2425;

                    if (verschil>0 && verschil < 122)
                    {
                        doorvragen = false;
                    }
                    else
                    {
                        Console.WriteLine("Dit is geen geldige geboortedatum. Probeer  opnieuw.");
                    }
                    
                }
                else
                {
                    Console.WriteLine("Dit is geen geldige datum. Probeer  opnieuw.");
                }
            }

            Console.WriteLine("\nWat is de datum van opname?");
            doorvragen = true;
            string datumOpname = "";
            while (doorvragen)
            {
                datumOpname = Console.ReadLine();
                if (IsDitEenDatum(datumOpname))
                {
                    doorvragen = false;
                }
                else
                {
                    Console.WriteLine("Dit is geen geldige datum. Probeer  opnieuw.");
                }
            }


            Console.WriteLine("\nWat is het rijksregisternummer?");
            doorvragen = true;
            string rijksregisternummer = "";
            while (doorvragen)
            {
                rijksregisternummer = Console.ReadLine();    //vb: "96080950338"
                if (IsDitEenRijksnummer(rijksregisternummer))
                {
                    doorvragen = false;
                }
                else
                {
                    Console.WriteLine("Dit is geen geldig rijksregisternummer. Probeer  opnieuw.");
                }
            }


            Console.WriteLine("\nWat is de naam van het ziekenhuis?");
            string[] ziekenhuisOpties = { "gasthuisberg", "uz jette", "azjp", "st-rafael", "heilig-hart" };

            //dit stukje is om de opties te tonen:
            Console.Write("Je kunt kiezen tussen: ");
            for (int i = 0; i < ziekenhuisOpties.Length; i++)
            {
                Console.Write(ziekenhuisOpties[i]);
                if (i == ziekenhuisOpties.Length - 1)  //als we op het einde zijn van de opties
                {
                    Console.Write("\n");
                }
                else
                {
                    Console.Write(", ");
                }
            }

            doorvragen = true;
            string naamZiekenhuis = "";
            while (doorvragen)
            {
                naamZiekenhuis = Console.ReadLine();
                if (ArrayBevat(naamZiekenhuis, ziekenhuisOpties) == true)
                {
                    doorvragen = false;
                }
                else
                {
                    Console.WriteLine("Dit is geen geldig ziekenhuis.");
                }
            }

            Console.WriteLine("\nWat is de status van de patient?");
            string[] opties = { "ziek","intensieve","genezen","dood" };
            
            //dit stukje is om de opties te tonen:
            Console.Write("Je kunt kiezen tussen: ");
            for (int i = 0; i < opties.Length; i++)
            {
                Console.Write(opties[i]);
                if (i == opties.Length - 1)  //als we op het einde zijn van de opties
                {
                    Console.Write("\n");
                }
                else
                {
                    Console.Write(", ");
                }
            }

            doorvragen = true;
            string status = "";
            while (doorvragen) 
            {
                status = Console.ReadLine();
                if (ArrayBevat(status, opties)==true)   //de '==true' is niet echt noodzakelijk
                {
                    doorvragen = false;
                }
                else
                {
                    Console.WriteLine("Dit is geen geldige optie.");
                }
            }

            Console.WriteLine("\nZijn er opmerkingen?");
            string opmerkingen = Console.ReadLine();

            using (StreamWriter writer = new StreamWriter("database.txt", true))	//true weglaten om het bestand te overschrijven
            {
                writer.WriteLine(naam+"|"+geboortedatum+ "|" + datumOpname+ "|" + rijksregisternummer + "|" + naamZiekenhuis + "|" + status + '|' + opmerkingen);
            }

            Console.WriteLine("\nPatient is toegevoegd in de database");
            Console.WriteLine();

        }


        private static void PatientAanpassen()
        {

            Console.WriteLine("\nWat is het rijksregisternummer van de patient waarbij je iets wilt veranderen?");
            string rijksregisternummer = Console.ReadLine();


            StreamReader reader = new StreamReader("database.txt");
            string regel = "";
            string nieuweDatabase = "";
            string[] eigenschappen = { "naam", "geboortedatum", "datum van opname", "rijksregisternummer", "naam van het ziekenhuis", "status", "opmerkingen" };

            bool gevonden = false;

            while (!reader.EndOfStream)
            {
                regel = reader.ReadLine();

                char[] delimiterChars = { '|' };
                string[] regelArray = regel.Split(delimiterChars);


                if (regelArray[3] == rijksregisternummer)    //hier checken we welke regel moet worden aangepast
                {
                    gevonden = true;
                    Console.WriteLine("\nOude gegevens zijn:");
                    RegelMooiWeergeven(regel);

                    Console.WriteLine("Wat wil je veranderen?");

                    
                    Console.Write("Je kunt kiezen tussen: ");
                    for (int i = 0; i < eigenschappen.Length; i++)
                    {
                        Console.Write(eigenschappen[i]);
                        if (i == eigenschappen.Length - 1)  //als we op het einde zijn van een regel
                        {
                            Console.Write("\n");
                        }
                        else
                        {
                            Console.Write(", ");
                        }
                    }



                    bool doorvragen = true;
                    string teVeranderen = "";
                    while (doorvragen)
                    {
                        
                        teVeranderen = Console.ReadLine();   //de gebruiker geeft iets in bv "naam"
                        if (ArrayBevat(teVeranderen,eigenschappen)) //hier checken we of we de string "naam" terugvinden in onze array 'eigenschappen'
                        {
                            //de gebruiker heeft iets goeds ingevoerd, dus we moeten niet meer doorvragen
                            doorvragen = false;
                        }
                        else
                        {
                            Console.WriteLine("Dit vinden we niet in onze database. Probeer nog eens.");
                        }
                    }




                    for (int i = 0; i < eigenschappen.Length; i++)
                    {
                        if (teVeranderen == eigenschappen[i])   //hier checken we welke eigenschap wordt aangepast
                        {
                            Console.WriteLine("\nGeef de verandering in.");
                            string verandering = "";

                            if (teVeranderen == "geboortedatum" || teVeranderen == "datum van opname")  //bij deze twee opties checken of het een datum is
                            {
                                doorvragen = true;
                                while (doorvragen)
                                {
                                    verandering = Console.ReadLine();
                                    if (IsDitEenDatum(verandering))
                                    {
                                        double verschil = (DateTime.Now - Convert.ToDateTime(verandering)).TotalDays / 365.2425;

                                        if (verschil > 0 && verschil < 122)
                                        {
                                            doorvragen = false;
                                        }
                                        else
                                        {
                                            Console.WriteLine("Dit is geen geldige datum. Probeer  opnieuw.");
                                        }


                                        
                                    }
                                    else
                                    {
                                        Console.WriteLine("Dit is geen datum. Probeer nog eens.");
                                    }
                                }
                            }

                            else if (teVeranderen == "naam")
                            {
                                doorvragen = true;
                                while (doorvragen)
                                {
                                    verandering = Console.ReadLine();
                                    if (verandering.Length > 3 && verandering.Contains(" "))
                                    {
                                        doorvragen = false;
                                    }
                                    else
                                    {
                                        Console.WriteLine("Dit is geen geldige naam. Probeer  opnieuw.");
                                    }
                                }
                            }

                            else if (teVeranderen == "rijksregisternummer")
                            {
                                doorvragen = true;
                                while (doorvragen)
                                {
                                    verandering = Console.ReadLine();
                                    if (IsDitEenRijksnummer(verandering))
                                    {
                                        doorvragen = false;
                                    }
                                    else
                                    {
                                        Console.WriteLine("Dit is geen geldig rijksregisternummer. Probeer  opnieuw.");
                                    }
                                }
                            }

                            else if (teVeranderen== "naam van het ziekenhuis")
                            {
                                string[] ziekenhuisOpties = { "gasthuisberg", "uz jette", "azjp", "st-rafael", "heilig-hart" };

                                //dit stukje is om de opties te tonen:
                                Console.Write("Je kunt kiezen tussen: ");
                                for (int j = 0; j < ziekenhuisOpties.Length; j++)
                                {
                                    Console.Write(ziekenhuisOpties[j]);
                                    if (j == ziekenhuisOpties.Length - 1)  //als we op het einde zijn van de opties
                                    {
                                        Console.Write("\n");
                                    }
                                    else
                                    {
                                        Console.Write(", ");
                                    }
                                }

                                doorvragen = true;
                                while (doorvragen)
                                {
                                    verandering = Console.ReadLine();
                                    if (ArrayBevat(verandering, ziekenhuisOpties) == true)
                                    {
                                        doorvragen = false;
                                    }
                                    else
                                    {
                                        Console.WriteLine("Dit is geen geldig ziekenhuis.");
                                    }
                                }
                            }

                            



                            else if (teVeranderen=="status")
                            {
                                
                                string[] opties = { "ziek", "intensieve", "genezen", "dood" };

                                //dit stukje is om de opties te tonen:
                                Console.Write("Je kunt kiezen tussen: ");
                                for (int j = 0; j < opties.Length; j++)
                                {
                                    Console.Write(opties[j]);
                                    if (j == opties.Length - 1)  //als we op het einde zijn van de opties
                                    {
                                        Console.Write("\n");
                                    }
                                    else
                                    {
                                        Console.Write(", ");
                                    }
                                }

                                doorvragen = true;
                                while (doorvragen)
                                {
                                    verandering = Console.ReadLine();
                                    if (ArrayBevat(verandering,opties))
                                    {
                                        doorvragen = false;
                                    }
                                    else
                                    {
                                        Console.WriteLine("Dit is geen geldige optie. Probeer nog eens.");
                                    }
                                }
                            }
                            else //dit is als we geen datum moeten ingeven
                            {
                                verandering = Console.ReadLine();
                            }


                            regelArray[i] = verandering;    //hier passen we de regelArray aan
                        }
                    }
                }

                for (int i = 0; i < regelArray.Length; i++)
                {
                    nieuweDatabase += regelArray[i];
                    if (i == regelArray.Length - 1)  //als we op het einde zijn van een regel
                    {
                        nieuweDatabase += "\n";
                    }
                    else
                    {
                        nieuweDatabase += "|";
                    }
                }

            }
            reader.Close();



            using (StreamWriter writer = new StreamWriter("database.txt"))	//nu overschrijven we de oude database
            {
                writer.Write(nieuweDatabase);
            }

            if (gevonden)
            {
                Console.WriteLine("\nPatient is aangepast in de database");
                Console.WriteLine();
            }
            else
            {
                Console.WriteLine("\nRijksregisternummer niet correct. We hebben deze patient niet gevonden.");
                Console.WriteLine();
            }

        }

        private static void PatientVerwijderen()
        {
            //enkel foutieve patienten worden verwijderd, gestorven mensen worden gwn aangepast in hun status
            Console.WriteLine("\nWat is het rijksregisternummer van de patient die je wilt verwijderen?");
            string rijksregisternummer = Console.ReadLine();


            StreamReader reader = new StreamReader("database.txt");
            string regel = "";
            string nieuweDatabase = "";
            
            bool gevonden = false;

            while (!reader.EndOfStream)
            {
                regel = reader.ReadLine();

                char[] delimiterChars = { '|' };
                string[] regelArray = regel.Split(delimiterChars);

                //als we een regel hebben met het juiste rijksnr dan zetten we deze regel niet in de nieuwe database

                if (regelArray[3] == rijksregisternummer)    //hier checken we welke regel moet worden verwijdert
                {
                    gevonden = true;
                    Console.WriteLine("\nDit is de patient die zal worden verwijderd:");
                    RegelMooiWeergeven(regel);

                    Console.WriteLine("Ben je zeker dat je deze patient wilt verwijderen? (ja/nee)");
                    string antwoord = Console.ReadLine();
                    if (antwoord != "ja")
                    {
                        Console.WriteLine("Oke, de patient wordt dus toch niet verwijderd. Kom terug wanneer je zeker bent.");

                        //dit stukje steekt de patient in de nieuwe database:
                        for (int i = 0; i < regelArray.Length; i++)
                        {
                            nieuweDatabase += regelArray[i];
                            if (i == regelArray.Length - 1)  //als we op het einde zijn van een regel
                            {
                                nieuweDatabase += "\n";
                            }
                            else
                            {
                                nieuweDatabase += "|";
                            }
                        }
                    }
                    else
                    {
                        //dit is het stukje voor als we de patient echt gaan wegdoen
                        Console.WriteLine("De patient is verwijderd uit de database.");
                    }

                }
                else  
                {
                    //dit stuk bouwt de nieuwe database weer op
                    for (int i = 0; i < regelArray.Length; i++)
                    {
                        nieuweDatabase += regelArray[i];
                        if (i == regelArray.Length - 1)  //als we op het einde zijn van een regel
                        {
                            nieuweDatabase += "\n";
                        }
                        else
                        {
                            nieuweDatabase += "|";
                        }
                    }
                }
            }
            reader.Close();

            using (StreamWriter writer = new StreamWriter("database.txt"))	//nu overschrijven we de oude database
            {
                writer.Write(nieuweDatabase);
            }

            if (!gevonden)
            {
                Console.WriteLine("Rijksregisternummer niet correct. We hebben deze patient niet gevonden.");
                Console.WriteLine();
            }
        }


        private static void OverzichtMaken()
        {

            

            //tellen hoeveel patienten er zijn:
            int aantalPatienten = File.ReadLines("database.txt").Count();
            


            
            StreamReader reader = new StreamReader("database.txt");
            string regel = "";

            DateTime[] geboortedata = new DateTime[aantalPatienten];
            DateTime[] opnamedata = new DateTime[aantalPatienten];
            string[] statussen = new string[aantalPatienten];
            string[] patientZiekenhuisArray = new string[aantalPatienten];
            

            

            int index = 0;
            while (!reader.EndOfStream)
            {
                regel = reader.ReadLine();

                char[] delimiterChars = { '|' };
                string[] regelArray = regel.Split(delimiterChars);

                geboortedata[index] = Convert.ToDateTime(regelArray[1]);
                opnamedata[index] = Convert.ToDateTime(regelArray[2]);
                statussen[index] = regelArray[5];
                patientZiekenhuisArray[index] = regelArray[4];





                index++;
            }
            reader.Close();


            //gemiddelde leeftijd berekenen:
            double[] verschillen = new double[aantalPatienten];
            
            for (int i = 0; i < geboortedata.Length; i++)
            {
                verschillen[i] = (DateTime.Now - geboortedata[i]).TotalDays / 365.2425;
            }


            //tellen aantal nieuw opgenomen:
            int nieuwOpgenomen = 0;
            for (int i = 0; i < opnamedata.Length; i++)
            {
                if (DateTime.Now.Date == opnamedata[i].Date) 
                {
                    nieuwOpgenomen++;
                }
            }

            //tellen aantal van elke status:
            string[] opties = { "ziek", "intensieve", "genezen", "dood" };
            int aantalZiek = 0;
            int aantalIntensieve = 0;
            int aantalGenezen = 0;
            int aantalDood = 0;


            for (int i = 0; i < statussen.Length; i++)
            {
                if (statussen[i]==opties[0])
                {
                    aantalZiek++;
                }
                else if (statussen[i] == opties[1])
                {
                    aantalIntensieve++;
                }
                else if (statussen[i] == opties[2])
                {
                    aantalGenezen++;
                }
                else if (statussen[i] == opties[3])
                {
                    aantalDood++;
                }
                else
                {
                    Console.WriteLine("ERROR! GROTE PROBLEMEN!!!!");
                }
            }

            //aantal patienten per ziekenhuis
            string[] ziekenhuisOpties = { "gasthuisberg", "uz jette", "azjp", "st-rafael", "heilig-hart" };
            int[] ziekenhuisTellers = new int[ziekenhuisOpties.Length];   //elke ziekenhuis heeft hier een aparte teller vb: {0,0,0}

            for (int i = 0; i < patientZiekenhuisArray.Length; i++) //we doorlopen de array van de patienten hun zieknhuizen vb : {azjp, azjp,gasthuisberg,gasthuisberg,gasthuisberg, ...}
            {
                for (int j = 0; j < ziekenhuisOpties.Length; j++)
                {
                    if (patientZiekenhuisArray[i] == ziekenhuisOpties[j])
                    {
                        ziekenhuisTellers[j]++;
                    }
                }
            }


            //afdrukken van het overzicht:
            string padding = "             ";

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(padding+"==== Overzicht ====");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(padding + "Totaal aantal besmettingen: ".PadRight(40) + aantalPatienten);
            Console.WriteLine(padding + "Gemiddelde leeftijd: ".PadRight(40) + Gemiddelde(verschillen));
            Console.WriteLine(padding + "Aantal nieuw opgenomen patienten is: ".PadRight(40) + nieuwOpgenomen);
            Console.WriteLine(padding + "Aantal op niet-intensieve afdeling: ".PadRight(40) + aantalZiek);
            Console.WriteLine(padding + "Aantal op de intensieve afdeling: ".PadRight(40) + aantalIntensieve);
            Console.WriteLine(padding + "Aantal genezen: ".PadRight(40) + aantalGenezen);
            Console.WriteLine(padding + "Aantal overleden: ".PadRight(40) + aantalDood);

            Console.WriteLine();
            for (int i = 0; i < ziekenhuisOpties.Length; i++)
            {
                Console.WriteLine(padding + ("Aantal patienten in " + ziekenhuisOpties[i] +": ").PadRight(40) + ziekenhuisTellers[i]);
            }
            

            


            Console.WriteLine("\n\nWil je terug naar het menu? (ja/nee)");
            while (true)
            {
                string antwoord = Console.ReadLine();
                if (antwoord == "ja")
                {
                    break;
                }
                else
                {
                    Environment.Exit(0);
                }
            }


        }




        static void Main(string[] args)
        {

            /*
            
            stap 1: menu     V
            stap 2: toevoegen (database opzetten)   V
            stap 3: aanpassen  V
            stap 4: verwijderen  V
            stap 5: overzicht   V
            stap 6: meer gegevensvalidatie   V
            stap 7: mooi maken    V



             */

            Console.ForegroundColor = ConsoleColor.White;   //dit is duidelijker te lezen

            bool doorgaan = true;
            while (doorgaan)
            {
                string padding = "             ";
                Console.WriteLine();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(padding+"===== Menu =====");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(padding + "1: Patient toevoegen");
                Console.WriteLine(padding + "2: Patient aanpassen");
                Console.WriteLine(padding + "3: Patient verwijderen");
                Console.WriteLine(padding + "4: Overzicht");
                Console.WriteLine(padding + "5: Sluit de applicatie");
                
                Console.WriteLine("\nKies het nummer van 1 van deze opties:");


                bool doorvragen = true;
                string keuze = "";
                int keuzegetal = -1234;

                while (doorvragen)
                {
                    keuze = Console.ReadLine();
                    try
                    {
                        keuzegetal = Convert.ToInt32(keuze);
                        if(keuzegetal > 0 && keuzegetal <= 5)
                        {
                            doorvragen = false;
                        }
                        else
                        {
                            Console.WriteLine("Error. Te groot of te klein getal. Kies 1 van de opties");
                        }
                    }
                    catch
                    {
                        //je hebt geen getal ingevoerd
                        Console.WriteLine("Error. Je moet een getal invoeren. Kies 1 van de opties");
                    }
                }


                switch (keuze)
                {
                    case "1":
                        Console.WriteLine("Je wilt dus een patient toevoegen.");
                        PatientToevoegen();
                        break;

                    case "2":
                        Console.WriteLine("Je wilt dus een patient aanpassen.");
                        PatientAanpassen();
                        break;

                    case "3":
                        Console.WriteLine("Je wilt dus een patient verwijderen.");
                        PatientVerwijderen();
                        break;

                    case "4":
                        if (System.IO.File.Exists("database.txt"))
                        {
                            Console.WriteLine("Hier is het overzicht:\n");
                            OverzichtMaken();
                        }
                        else
                        {
                            Console.WriteLine("Je moet eerst patienten in de database zetten!!");
                        }
                        
                        break;

                    case "5":
                        Console.WriteLine("Je wilt dus sluiten?");
                        string antwoord = Console.ReadLine();
                        if (antwoord == "ja")
                        {
                            Environment.Exit(0);
                        }
                        
                        
                        break;

                    default:    //normaal komen we hier nooit
                        Console.WriteLine("Error. Er is iets misgelopen :(");
                        Environment.Exit(0);
                        break;
                }
            }



            Console.ReadLine();

        }
    }
}
